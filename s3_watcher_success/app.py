import json
import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

def lambda_handler(event, context):
    logger.debug("S3 Watcher Success")
    logger.debug(f"Event: {event}")

    try:
        record = event['requestPayload']['Records'][0]
        eventSource = record["eventSource"]
        bucket = record["s3"]["bucket"]["name"]
        key = record["s3"]["object"]["key"]
        logger.debug(eventSource)
        logger.debug(bucket)
        logger.debug(key)

    except:
        pass


    return {
        'statusCode': 200,
        'body': json.dumps('S3 Watcher Success Complete')
    }
